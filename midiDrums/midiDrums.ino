/*
 * Copyright (c) 2015 Evan Kale
 * Email: EvanKale91@gmail.com
 * Website: www.ISeeDeadPixel.com
 *          www.evankale.blogspot.ca
 *
 * This file is part of ArduinoMidiDrums.
 *
 * ArduinoMidiDrums is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define NUM_PIEZOS 9 /*Количество пьезоэлементов*/
#define NUM_PEDALS 3 /*Количество датчиков для педалей (герконов) */
//========================= входы
#define CRASH           A0
#define RING            A2
#define RIDE            A1
#define HIGHHAT         A3
#define TOM1            A4
#define SNARE           A5
#define SNARE_RIM       A6
#define TOM2            A7
#define TOM3            A8
#define PEDAL_HIGHHAT   A13
#define PEDAL_BASS_2    A12
#define PEDAL_BASS_1    A14
//===============================

//========================= порог срабатывания
//все, что меньше этого значения принимается за 0
#define CRASH_THRESHOLD     30
#define HIGHHAT_THRESHOLD   30
#define RIDE_THRESHOLD      30
#define RING_THRESHOLD      30
#define SNARE_THRESHOLD     30
#define SNARE_RIM_THRESHOLD 30
#define TOM1_THRESHOLD      30
#define TOM2_THRESHOLD      30
#define TOM3_THRESHOLD      30
//===============================


//========================= номера типов (то, что посылается в Serial)
#define CRASH_TYPE                  0
#define HIGHHAT_TYPE                1
#define RIDE_TYPE                   2
#define RING_TYPE                   3
#define PEDAL_BASS_1_TYPE           4
#define PEDAL_BASS_2_TYPE           5
#define PEDAL_HIGHHAT_PRESS_TYPE    6
#define PEDAL_HIGHHAT_RELEASE_TYPE  7
#define SNARE_TYPE                  8
#define SNARE_RIM_TYPE              9
#define TOM1_TYPE                   10
#define TOM2_TYPE                   11
#define TOM3_TYPE                   12
//===============================

#define SERIAL_RATE 115200 /*Скорость порта*/

//В миллисекундах
#define SIGNAL_BUFFER_SIZE 100
#define PEAK_BUFFER_SIZE 30
#define MAX_TIME_BETWEEN_PEAKS 20
#define MIN_TIME_BETWEEN_NOTES 50
#define MIN_TIME_BETWEEN_NOTES_PLATE 100 /*Для тарелок*/
#define MIN_TIME_BETWEEN_PEDALS 200

//========Кольцевые буферы для сохранения аналоговых сигналов, пиков и временных отметок
short currentSignalIndex[NUM_PIEZOS];
short currentPeakIndex[NUM_PIEZOS];
unsigned short signalBuffer[NUM_PIEZOS][SIGNAL_BUFFER_SIZE];
unsigned short peakBuffer[NUM_PIEZOS][PEAK_BUFFER_SIZE];

boolean noteReady[NUM_PIEZOS];
unsigned short noteReadyVelocity[NUM_PIEZOS];
boolean isLastPeakZeroed[NUM_PIEZOS];

unsigned long lastPeakTime[NUM_PIEZOS];
unsigned long lastNoteTime[NUM_PIEZOS];
unsigned long lastPedalTime[NUM_PIEZOS];
//==============================================

bool state[NUM_PEDALS]; //Состояния педалей (нажата/поднята)


//========Функции
void initPieso(short i);
void initPedal(short i);
void pollPiezo(short i, unsigned long currentTime, short pin, short type, short threshold);
void pollPedalPress(short i, short pin, short typePressed);
void pollPedalPressRelease(short i, short pin, short typePressed, short typeReleased);
void recordNewPeak(short type, short slot, short newPeak);
void noteFire(unsigned short type, unsigned short velocity);
void noteFire(unsigned short type, unsigned short velocity);


//Инициализация переменных для пьезоэлемента
void initPieso(short i) {
    currentSignalIndex[i] = 0;
    currentPeakIndex[i] = 0;
    memset(signalBuffer[i],0,sizeof(signalBuffer[i]));
    memset(peakBuffer[i],0,sizeof(peakBuffer[i]));
    noteReady[i] = false;
    noteReadyVelocity[i] = 0;
    isLastPeakZeroed[i] = true;
    lastPeakTime[i] = 0;
    lastNoteTime[i] = 0;
}

//Инициализация переменных для педали
void initPedal(short i) {
    state[i] = LOW; //Начальное положение - педаль поднята
}

//Начальная инициализация
void setup()
{
  //Установка скорости порта
  Serial.begin(SERIAL_RATE);
  //включаем подтягивающие резисторы
  digitalWrite(PEDAL_BASS_2, HIGH);
  digitalWrite(PEDAL_HIGHHAT, HIGH);
  digitalWrite(PEDAL_BASS_1, HIGH);

  //инициализация переменных для каждого пьезоэлемента
  for(int i=0; i<NUM_PIEZOS; i++) {
    initPieso(i);
  }

  //инициализация переменных для каждой педали
  for(int i=0; i<NUM_PEDALS; i++) {
    initPedal(i);
  }
}

//опрос аналогового входа (к которому подключен пьезоэлемент)
//аргументы: 1) индекс переменной в буфере
//           2) текущее время в мс
//           3) номер входа
//           4) Номер типа, который посылается в Serial, когда получен пик
//           5) Пороговое значение
void pollPiezo(short i, unsigned long currentTime, short pin, short type, short threshold) {
    //get a new signal from analog read
    unsigned short newSignal = analogRead(pin);
    signalBuffer[i][currentSignalIndex[i]] = newSignal;
    
    //if new signal is 0
    if(newSignal < threshold)
    {
      if(!isLastPeakZeroed[i] && (currentTime - lastPeakTime[i]) > MAX_TIME_BETWEEN_PEAKS)
      {
        recordNewPeak(type, i, 0);
      }
      else
      {
        //get previous signal
        short prevSignalIndex = currentSignalIndex[i]-1;
        if(prevSignalIndex < 0) prevSignalIndex = SIGNAL_BUFFER_SIZE-1;        
        unsigned short prevSignal = signalBuffer[i][prevSignalIndex];
        
        unsigned short newPeak = 0;
        
        //find the wave peak if previous signal was not 0 by going
        //through previous signal values until another 0 is reached
        while(prevSignal >= threshold)
        {
          if(signalBuffer[i][prevSignalIndex] > newPeak)
          {
            newPeak = signalBuffer[i][prevSignalIndex];        
          }
          
          //decrement previous signal index, and get previous signal
          prevSignalIndex--;
          if(prevSignalIndex < 0) prevSignalIndex = SIGNAL_BUFFER_SIZE-1;
          prevSignal = signalBuffer[i][prevSignalIndex];
        }
        
        if(newPeak > 0)
        {
          recordNewPeak(type, i, newPeak);
        }
      }
  
    }
        
    currentSignalIndex[i]++;
    if(currentSignalIndex[i] == SIGNAL_BUFFER_SIZE) currentSignalIndex[i] = 0;
}

//опрос аналогового входа (к которому подключена педаль)
//аргументы: 1) индекс переменной в буфере
//           2) номер входа
//           3) Номер типа, который посылается в Serial, когда педаль нажимается
void pollPedalPress(short i, unsigned long currentTime, short pin, short typePressed) {
  //если педаль нажата (state[i] == HIGH) и на входе лог 0 (<1000)
  if(state[i] == HIGH && analogRead(pin) < 1000) {
    //переводим педаль в состояние "отпущена"
    state[i] = LOW;
  }
  //если педаль отпущена (state[i] == HIGH) и на входе лог 1 (>1000)
  //и со времени последнего нажати прошло >MIN_TIME_BETWEEN_PEDALS миллисекунд
  if(state[i] == LOW && analogRead(pin) >= 1000 &&
     ((currentTime - lastPedalTime[i]) > MIN_TIME_BETWEEN_PEDALS)) {
    //переводим педаль в состояние "нажата"
    state[i] = HIGH;
    //Посылаем в Serial код педали
    noteFire(typePressed, 1023);
    lastPedalTime[i] = currentTime;
  }
}

//опрос аналогового входа (к которому подключена педаль)
//Тут то же самое, только посылаем код и при отпускании педали (нужно для хайхета)
//аргументы: 1) индекс переменной в буфере
//           2) номер входа
//           3) Номер типа, который посылается в Serial, когда педаль нажимается
//           4) Номер типа, который посылается в Serial, когда педаль отпускается
void pollPedalPressRelease(short i, unsigned long currentTime, short pin, short typePressed, short typeReleased) {
  if(state[i] == HIGH && analogRead(pin) < 1000) {
    state[i] = LOW;
    //Посылаем в Serial код педали
    noteFire(typeReleased, 1023);
  }
  if(state[i] == LOW && analogRead(pin) >= 1000) {
    state[i] = HIGH;
    //Посылаем в Serial код педали
    noteFire(typePressed, 1023);
  }
}

//Основной цикл программы
void loop()
{
  unsigned long currentTime = millis();//текущее время в миллисекундах

  //опрос пьезоэлементов
  pollPiezo(0, currentTime, CRASH,      CRASH_TYPE,     CRASH_THRESHOLD);
  pollPiezo(1, currentTime, RING,       RING_TYPE,      RING_THRESHOLD);
  pollPiezo(2, currentTime, RIDE,       RIDE_TYPE,      RIDE_THRESHOLD);
  pollPiezo(3, currentTime, HIGHHAT,    HIGHHAT_TYPE,   HIGHHAT_THRESHOLD);
  pollPiezo(4, currentTime, SNARE,      SNARE_TYPE,     SNARE_THRESHOLD);
  pollPiezo(5, currentTime, SNARE_RIM,  SNARE_RIM_TYPE, SNARE_RIM_THRESHOLD);
  pollPiezo(6, currentTime, TOM1,       TOM1_TYPE,      TOM1_THRESHOLD);
  pollPiezo(7, currentTime, TOM2,       TOM2_TYPE,      TOM2_THRESHOLD);
  pollPiezo(8, currentTime, TOM3,       TOM3_TYPE,      TOM3_THRESHOLD);

  //опрос герконов
  pollPedalPress(0, currentTime, PEDAL_BASS_1, PEDAL_BASS_1_TYPE);
  pollPedalPress(1, currentTime, PEDAL_BASS_2, PEDAL_BASS_2_TYPE);
  pollPedalPressRelease(2, currentTime, PEDAL_HIGHHAT, PEDAL_HIGHHAT_PRESS_TYPE, PEDAL_HIGHHAT_RELEASE_TYPE);
}

//Запись пиков в буфер и определение пика для отправки в Serial (см. ниже коммент от автора про 3 случая) 
void recordNewPeak(short type, short slot, short newPeak)
{
  isLastPeakZeroed[slot] = (newPeak == 0);
  
  unsigned long currentTime = millis();
  lastPeakTime[slot] = currentTime;
  
  //new peak recorded (newPeak)
  peakBuffer[slot][currentPeakIndex[slot]] = newPeak;
  
  //1 of 3 cases can happen:
  // 1) note ready - if new peak >= previous peak
  // 2) note fire - if new peak < previous peak and previous peak was a note ready
  // 3) no note - if new peak < previous peak and previous peak was NOT note ready
  
  //get previous peak
  short prevPeakIndex = currentPeakIndex[slot]-1;
  if(prevPeakIndex < 0) prevPeakIndex = PEAK_BUFFER_SIZE-1;        
  unsigned short prevPeak = peakBuffer[slot][prevPeakIndex];

  int minTimeBetweenNotes;

  //если тип - тарелка
  if( (type == CRASH_TYPE) || (type == RIDE_TYPE)
      || (type == RING_TYPE) || (type == HIGHHAT_TYPE) ) {
    //устанавливается minTimeBetweenNotes для тарелок
    minTimeBetweenNotes = MIN_TIME_BETWEEN_NOTES_PLATE;
  } else {
    minTimeBetweenNotes = MIN_TIME_BETWEEN_NOTES;
  }

  if(newPeak > prevPeak && (currentTime - lastNoteTime[slot]) > minTimeBetweenNotes)
  {
    noteReady[slot] = true;
    if(newPeak > noteReadyVelocity[slot])
      noteReadyVelocity[slot] = newPeak;
  }
  else if(newPeak < prevPeak && noteReady[slot])
  {
    noteFire(type, noteReadyVelocity[slot]); //Запись в Serial
    noteReady[slot] = false;
    noteReadyVelocity[slot] = 0;
    lastNoteTime[slot] = currentTime;
  }
  
  currentPeakIndex[slot]++;
  if(currentPeakIndex[slot] == PEAK_BUFFER_SIZE) currentPeakIndex[slot] = 0;  
}

//Отправляем в Serial тип барабана и громкость
void noteFire(unsigned short type, unsigned short velocity)
{
  velocity = velocity / 4;

  Serial.write(type);//println
  Serial.write(velocity);
}

